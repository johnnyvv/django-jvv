"""
These filters can be used in templates like {{ value | <name of filter> }}
There are some rules:
these filters need to be placed in <app dir>/templatetags directory, this is a package so make sure to create an empty file called __init__.py
touch <app dir>/templatetags/__init__.py 
inside that directory create a file with a naming that is reasonable, in my case filters.py
in the filters.py place your filters like beneath. 
The app that contain the filters. must be registred in the INSTALLED_APPS
every filter needs to be registred, see example underneath
server needs a restart after new template file
inside your template do the following:
{% load <name of your file> %}
{% load filters %}
{{ myvar | myfilter }}
"""
from django import template
from django.template.defaultfilters import stringfilter
from hashlib import sha512

register = template.Library()

@register.filter
def generate_hash(value):
    return sha512(value.encode('utf-8')).hexdigest()

@register.filter
@stringfilter #makes sure its all a string
def low_upper(value):
    value = value[:(len(value) // 2)].lower() + value[(len(value) //2):].upper()
    print(value)
    return value
    
#if you dont use the @register.filter you can register the filter by using
#register.filter('<name of filter>', <name of filter, the def name>
#register.filter('generate_hash', generate_hash)
