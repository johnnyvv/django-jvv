"""
Lets say you need to access a variable in your context often, or always. You dont always want to 
include that in the context you give to a template, rather you can use context processors
beneath a simple example that calls a function to see how many users there are in the database
You can access this in your template by using {{key}} in this case {{user_count}}
Before you can use them, you need to register them in your settings.py in the array 
TEPLAMTES.OPTION.context_processors simple add each context processor in the array like:
'myusers.context_processors.user_count',
"""
def user_count(request):
    count = CustomUser.objects.count()
    return {"user_count": count}

def current_time(request):
    """simple return current date"""
    return {"current_time" : datetime.now().strftime('%H:%M:%S %d-%m-%Y')}