from datetime import date
from .models import User
from faker import Faker

def get_user(username):
    try:
        username_ret = User.objects.get(username__exact=username)
        return username_ret
    except User.DoesNotExist:
        return None

def get_all_users():
    return User.objects.all()    

def get_users_limited(limit=1):
    return User.objects.all()[:int(limit)]

def get_age_summary_dict():
    #remember the order, you will get the results in the same order!
    user_list = User.objects.values_list('username','reg_date').order_by('username')
    #oldest is based on most days since today

    user_dates = [user[1] for user in user_list]

    ret_dict = {"oldest": {"username": "x", "days": 0}, "youngest":{"username": "x", "days": 0},"average_age": get_average_age(user_dates)}
    today = date.today()#might as well remove this and put in the function below..

    for user in user_list:
        print(user[0])
        if  ret_dict['oldest']['username']: 
            if ret_dict['oldest']['days'] < (compare_age(today,user[1])).days:
                ret_dict['oldest']["username"] = user[0]
                ret_dict['oldest']["days"] = (today - user[1]).days
        if ret_dict['youngest']['username']:
            if ret_dict['youngest']['days'] == 0 or ret_dict['youngest']['days'] > (compare_age(today,user[1])).days:
                ret_dict['youngest']["username"] = user[0]
                ret_dict['youngest']["days"] = (today - user[1]).days
    return ret_dict
            
def search(search_type, value): #searchtype email last name username etc.. value the value that need to be searched for.
    if search_type == "fname":
        #might be nice to create a function for this with 
        value_ret = User.objects.get(first_name__exact=value)
        return value_ret
    elif search_type == "lname":
        value_ret = User.objects.get(last_name__exact=value)
        return value_ret
    elif search_type == "username":
        value_ret = User.objects.get(username__exact=value)
        return value_ret
    elif search_type == "email":
        value_ret = User.objects.get(email__exact=value)
        return value_ret
    else:
        pass

def compare_age(today,some_date):
    return today - some_date

def get_average_age(user_dates):
    today = date.today()
    #[print(user) for user in user_dates]    
    count_days_user = [(today - user).days for user in user_dates]    
    #print(sum(count_days_user))
    return int((sum(count_days_user) / len(count_days_user)) / 365)

def generate_users(gen_amount):
    #need to add some more try catch
    fakegen = Faker()
    try:
        for user in range(int(gen_amount)):
            first_name = fakegen.first_name()
            last_name = fakegen.last_name()
            username = fakegen.profile()['username']
            email = fakegen.email()
            reg_date = fakegen.date()
            User.objects.get_or_create(first_name=first_name,last_name=last_name,reg_date=reg_date,username=username,email=email)[0]
        return True
    except User.Error():
        return None
