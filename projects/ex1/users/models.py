from django.db import models

# Create your models here.

class User(models.Model):
    first_name = models.CharField(max_length=20, null=False)
    last_name = models.CharField(max_length=30, null=False)
    username = models.CharField(max_length=20,unique=True, null=False)
    email = models.EmailField(max_length=50,unique=True, null=False)
    reg_date = models.DateField()
    
    def __str__(self):
        return str(self.username)
