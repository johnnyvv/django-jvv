from django.shortcuts import render 
from django.http import HttpResponse,HttpRequest
from .db_actions import get_user,get_all_users,get_users_limited, get_age_summary_dict, generate_users, search
from . import forms

# Create your views here.

def index(request):
    for i in request:
        print(i)
    return HttpResponse("First view")


def search_user(request):
    form = forms.getUser()
    if request.method == "POST":
        if request.POST['choice_field'] == "username":
            user_set = search(request.POST['choice_field'],request.POST['search'])
            print(user_set)
            render_dict = {"html_title": "Search result","result": user_set}
            return render(request, 'users/result-single.html', render_dict)
#             return HttpResponse("Result:</ br>%s" % result.email)
#            return HttpResponse("Searching for username:%s" % request.POST['search'])
        elif request.POST['choice_field'] == "fname":
            user_set = search(request.POST['choice_field'],request.POST['search'])
            render_dict = {"html_title": "Search result","result": user_set}
            return render(request, 'users/result-single.html', render_dict)
        elif request.POST['choice_field'] == "lname":
            user_set = search(request.POST['choice_field'],request.POST['search'])
            render_dict = {"html_title": "Search result","result": user_set}
            return render(request, 'users/result-single.html', render_dict)
        elif request.POST['choice_field'] == "email":
            print("email")
            user_set = search(request.POST['choice_field'],request.POST['search'])
            render_dict = {"html_title": "Search result","result": user_set}
            return render(request, 'users/result-single.html', render_dict)
        else:
            return HttpResponse("An error occured. Please google")
    else:
        return render(request,'users/form.html', {'form':form})

def print_user(request, username):
    username_ret = get_user(username)
    print(username_ret)
    if username_ret:
        #return HttpResponse("found user")
        return HttpResponse("Found user %s. to contact by email: %s" % (username_ret.username,username_ret.email))
    else:
        return HttpResponse("<h1>user {} not found!".format(username))

def print_all_users(request):
    users = get_all_users()
    x=[]
    #[print(user.username) for user in users]
    [x.append(user.username) for user in users]
    return HttpResponse(x)

def generate_user_list(request):
    user_set = get_all_users()
    render_dict = {"html_title": "List of users","user_set": user_set}
    return render(request, 'users/list.html', render_dict)

def limit_user_list(request,limit):
    user_set = get_users_limited(limit)
    render_dict = {"html_title": limit ,"user_set": user_set}
    #return HttpResponse("ok")
    return render(request, 'users/list.html', render_dict)

def get_age_summary(request):
    a = get_age_summary_dict()
    print(request.GET.get('username','empty'))
    #print(request.GET['user'])
    render_dict = {"html_title": "Age overview", "user_set": a}
    print(a)
    return render(request, 'users/age.html', render_dict)
    #We only want a dictionary with: youngest, oldest, average. That is what we print.

def gen_users(request, gen_amount):
    result = generate_users(gen_amount)
    if result:
        return HttpResponse("Succesfully added {} to the database.".format(gen_amount))
    else:
        return HttpResponse("Failed to generate database data.")
