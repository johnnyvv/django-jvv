from django import forms

class getUser(forms.Form):
    search_options=[('username','username'),('email','email'),('fname','first name'),('lname','last name')]
    choice_field = forms.ChoiceField(choices=search_options,widget=forms.Select(attrs={'class':'btn btn-primary dropdown-toggle'}))

    #choice_field = forms.ChoiceField(widget=forms.RadioSelect, choices=search_options)
    search = forms.CharField(help_text="Enter the  username you want to search.",widget=forms.TextInput(attrs={'class':'form-control'}))
