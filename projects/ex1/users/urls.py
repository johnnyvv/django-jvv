from django.urls import path,re_path
from . import views 
urlpatterns = [
    path('', views.index),
    re_path(r'^user/(?P<username>[\w.@+-]+)$',views.print_user),
    path('all',views.print_all_users),
    re_path(r'^list$', views.generate_user_list),
    re_path(r'^list/(?P<limit>[0-9])',views.limit_user_list),
    re_path(r'^age', views.get_age_summary),
    re_path(r'^generate/(?P<gen_amount>[0-9])', views.gen_users),
    re_path(r'^search', views.search_user),
        ]
