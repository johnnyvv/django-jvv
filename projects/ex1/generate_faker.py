import os 
os.environ.setdefault('DJANGO_SETTINGS_MODULE','ex1.settings')
import django
django.setup()

##generate with faker
import random
from users.models import User
from faker import Faker

fakegen = Faker()

def check_num_rows():
    pass


def populate(n=5):
    for run in range(n):
        
        first_name = fakegen.first_name()
        last_name = fakegen.last_name()
        username = fakegen.profile()['username']
        email = fakegen.email()
        reg_date = fakegen.date()

        create_user = User.objects.get_or_create(first_name=first_name,last_name=last_name,reg_date=reg_date,username=username,email=email)[0]

"""
def populate(n=5):
    for entry in range(n):
        #get the topic
        top = add_topic()
        #create fake data
        fake_url = fakegen.url()
        fake_date = fakegen.date()
        fake_name = fakegen.company()

        #create new webpage entry
        webpage = Webpage.objects.get_or_create(topic=top, url=fake_url, name=fake_url)[0]

        #create fake accessrecord
        acc_rec = AccessRecord.objects.get_or_create(name=webpage, date=fake_date)[0]
"""
if __name__ == '__main__':
    print("Generating and populating...")
    populate(10)
    print("Done")
