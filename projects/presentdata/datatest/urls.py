from django.urls import path, re_path
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    re_path(r'^$',TemplateView.as_view(template_name="datatest/index.html", extra_context={'html_title': 'Company','msg':'Welcome to data index',})),
    re_path(r'^static$', TemplateView.as_view(template_name='datatest/dt_static.html')),
    re_path(r'^test$', views.datatables_dynamic_json),
    re_path(r'^db$', views.datatables_customer),

]