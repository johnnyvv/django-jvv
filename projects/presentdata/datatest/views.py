from django.shortcuts import render
from django.http import HttpResponse

from json import dumps as json_dumps

from devices.models import Customer
# Create your views here.


def datatables_dynamic_json(request):
    dt_data = [
        {'name':'johnny','age':28,'city':'Purmerend','eyes':'green',},
        {'name':'tina','age':35,'city':'Bandung','eyes':'brown',},
        {'name':'roos','age':58,'city':'Purmerend','eyes':'blueies',},
    ]    
    template = 'datatest/dt_dynamic_json.html'
    dt_data_headers = list(dt_data[0].keys()) #so wont generate if dt_data is empty

    print(dt_data_headers)
    return render(request, template, {
        'datatables_array': json_dumps(dt_data),
        'datables_headers':dt_data_headers,
    })

def datatables_customer(request):
    #dt_data = list(Customer.objects.values())
    dt_data = list(Customer.objects.values('company','contact_person','contact_email'))

    template = 'datatest/dt_model_json.html'
    if isinstance(dt_data[0], dict):
        dt_data_headers = list(dt_data[0].keys()) #so wont generate if dt_data is empty
    else:
        return HttpResponse("Failed to get table headers, since value is not a dictionary.")
    #print(dt_data)
    #return HttpResponse("abc")
    return render(request, template, {
        'datatables_array': json_dumps(dt_data),
        'datables_headers':dt_data_headers,
    })
