from .models import CustomUser
from django.db import IntegrityError

def get_users():
    return CustomUser.objects.all()

def get_all_users():
    return CustomUser.objects.all()

def add_user(username,password):
    #here we add a user to the database and do  some error checking
    #for now, we only check if the username is already in the database
    try:
        CustomUser.objects.create_user(username=username,password=password)
        return {"success": True, "msg": "Succesfully added user %s" % (username,)}
    except IntegrityError:
        return {"success": False, "msg":"User %s already exists." % (username,)}

def get_bad_password_users(str_to_find):
    #just a call to remove passwords not starting with a string e.g. argon2 
    #str_to_find="argon2"
    #the function here is exclude, here you exclude the users where the pw starts with str_to_find
    users_found = CustomUser.objects.exclude(password__startswith=str_to_find)
    print(users_found)
    #if queryset is 0, it's False
    #if len(users_found) == 0: 
    if users_found:
        return users_found
    else:
        return False #might change this to other value 

def get_user(username):
    user_found = CustomUser.objects.get(username=username)
    if user_found:
        return user_found
    else:
        return False

def delete_user(username):
    user_queryset = get_user(username)
    if user_queryset:
        #dont call save, because it will add the user back to the db
        user_queryset.delete()
        return True
    else:
        False

def update_user(**kwargs):
    print(kwargs)
    user = None
    updated = False
    if 'username' in kwargs:
        user = CustomUser.objects.get(username=kwargs['username'])
    else:
        return updated #makes no sense to continue
    print(user.username)
    if 'first_name' in kwargs:
        user.first_name =  kwargs['first_name']
        updated = True
    if 'last_name' in kwargs:
        user.last_name = kwargs['last_name']
        updated = True
    if 'email' in kwargs:
        user.email = kwargs['email']
        updated = True
    if updated:
        print(type(user))
        user.save()
    return updated



