from django import template
from django.template.defaultfilters import stringfilter
from hashlib import sha512

register = template.Library()


@register.filter
def generate_hash(value):
    return sha512(value.encode('utf-8')).hexdigest()

@register.filter
@stringfilter
def low_upper(value):
#    value = str(value)
    value = value[:(len(value) // 2)].lower() + value[(len(value) //2):].upper()
    print(value)
    return value
#register.filter('generate_hash', generate_hash)
