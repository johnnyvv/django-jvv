from myusers.models import CustomUser
from datetime import datetime

def user_count(request):
    """
    This make sure that the dict key user_count can be accessed from any template.
    This is useful for data you always need to have, e.g. user count or count of some value...
    just refer to this in template with the dict key you give @ return {{user_count}} #need to be unique so think about a good name

    """
    count = CustomUser.objects.count()
    return {"user_count": count}

def current_time(request):
    """simple return current date"""
    a = datetime.now().strftime('%H:%M:%S %d-%m-%Y')
    print("hello")
    return {"current_time" : a}

