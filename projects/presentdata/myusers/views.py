from django.shortcuts import render, redirect
from django.http import HttpResponse
from .databasedefs import get_users,get_all_users, add_user, get_bad_password_users, delete_user, update_user,get_user
from . import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login,logout,authenticate
from .models import CustomUser
from django.contrib.auth.hashers import check_password
from django.contrib.auth.decorators import login_required

# Create your views here.
def index(request):
    return render (request, 'myusers/extend.html', {'html_title': '*Index page*', 'message': 'This is the index page.'})
    #return render(request, 'myusers/home.html', {'html_title':'home'})
    #return HttpResponse("Hello there.")

def get_usernames(request):
    usernames = get_users()
    ret_dict = {"html_title": "users", "users": usernames}
    return render(request, 'myusers/listusers.html', ret_dict)
    return HttpResponse(usernames)

def add_user_form(request):
    if request.method == "POST":
        form = forms.CreateUser(request.POST)
        if form.is_valid():
            print("form is valid")
            print("username: %s" % form.cleaned_data['username'])
            username =  form.cleaned_data['username']
            print("password: %s" % form.cleaned_data['password'])
            password = form.cleaned_data['password']
            #adding user
            #CustomUser.objects.create_user(username=username,password=password)
            #ret_dict = {"html_title" :"Succesfully added user", "succes_msg":True, "user_username":form.cleaned_data['username']}
            user_add_result = add_user(username,password)
            context = user_add_result
            context['form'] = forms.CreateUser()
            return render(request, 'myusers/adduserform.html', context)
    form = forms.CreateUser()
    ret_dict = {"html_title" : "Add user", "form" : form}
    return render(request, 'myusers/adduserform.html', ret_dict)

def user_login(request):
    if request.method == "POST":
        print("Posting")
        form = forms.LoginUserForm(data=request.POST)
        if form.is_valid():
            username =  form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username,password=password)
            if user:
                if user.is_active:
                    login(request,user)
                    return HttpResponse("check")
            else:
                return HttpResponse("Login failed")
            #user = CustomUser.objects.get(username__exact=username)
            #print(check_password(password,user.password))
            #if check_password(password,user.password):
#                return HttpResponse("You are now logged in")
        else:
            print(form.error)
            return HttpResponse("Errrr in form")
    form = forms.LoginUserForm()
    ret_dict = {"html_title": "Login", "form":form}
    return render(request, 'myusers/login.html', ret_dict)

def user_logout(request):
    if request.user.is_authenticated:
        logout(request)
        return redirect('/') 
        #return HttpResponse("You could logout")
    else:
        return HttpResponse("Maaan, you are not even logged in..")
def home(request):
    return HttpResponse("Welcome home")

def listusers(request):
    if request.user.is_authenticated:
        users = get_all_users()
        ret_dict = {"html_title": "List of users", "users":users}
        return render(request, 'myusers/listusers.html',ret_dict)
    else:
        return redirect('/login')
def delusers(request):
    if request.method == "POST":
        print(type(request.POST.getlist('delete')))
        to_del = request.POST.getlist('delete')
        deleted_users = []
        for del_user in to_del:
            delete_user(del_user)
            deleted_users.append(del_user)
        ret_dict = {"html_title":"Delete user", "deleted_users":deleted_users, "users":get_all_users()} 
        return render(request, 'myusers/delusers.html', ret_dict)
#        return HttpResponse(users_deleted)
    if request.user.is_authenticated:
        users = get_all_users()
        ret_dict = {"html_title": "List of users", "users":users}
        return render(request, 'myusers/delusers.html',ret_dict)
    else:
        return redirect('/login')

@login_required(login_url='/')
def password_check(request):
    result = get_bad_password_users("argon2")    
    ret_dict = {"html_title" : "Password check", "users": result}
    if not result:
        return HttpResponse("All passwords are hashed")
    else:
        return render (request, 'myusers/listusers.html', ret_dict)
#        return HttpResponse("Found %s user(s) with bad passwords" % (len(result),))

#just checking if this works, make sure you have some handling for the url /next=.. did not work this time!
@login_required(login_url='/login')
def my_secret_page(request):
    #if request.user.is_authenticated:
    return HttpResponse("This is my secret, exciting?")

def del_user(request, username):
    print("about to delete %s" % (username,))
    delete = delete_user(username)
    print(delete)
    if delete:
        return HttpResponse("Succesfully deleted user %s" % (username,))
    else:
        return HttpResponse("Failed to delete user %s" % (username,))

def whoami(request):
    print(CustomUser._meta.app_label)
    print(CustomUser._meta.db_table)
    print(CustomUser._meta)
    if request.user.is_authenticated:
        user = get_user(request.user.username)  
        ret_dict = {"html_title":"User info", "users": user}
        return render(request, 'myusers/usertable.html', ret_dict)
    else:
        return redirect('/login')

def count_user_view(request):
    return render(request, 'myusers/context_processor.html',{"html_title": "user count"})

@login_required
def edit_profile(request):
    #edit simple info like first/last name and email adress
    #for this we need a form
    if request.method == "POST":
        valid_items = ["first_name","last_name","email"]
        update_dict = {"username":request.user.username}
        for item in request.POST:
            if item in valid_items and request.POST[item]:
                update_dict[item] = request.POST[item]
        #print(update_dict)
        #now call the update_user method
        updated = update_user(**update_dict)
        if updated:
            return render(request,'myusers/editprofile.html', {"html_title": "Edit profile", "form": forms.EditProfile, "message": "Saved your changes" })
    return render(request,'myusers/editprofile.html', {"html_title": "Edit profile", "form": forms.EditProfile})


