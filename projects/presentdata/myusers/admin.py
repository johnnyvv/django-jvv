from django.contrib import admin
from devices.models import Customer, NetworkDevice
# Register your models here.

admin.site.register(NetworkDevice)
admin.site.register(Customer)