from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.index),
    re_path(r'^users', views.get_usernames),
    re_path(r'^adduserformmodel', views.add_user_form,name='adduserform'),
    re_path(r'^home',views.home, name='home'),
    re_path(r'^login',views.user_login, name='login'),
    re_path(r'^logout', views.user_logout, name='logout'),
    re_path(r'^secret', views.my_secret_page, name='secret'),
    re_path(r'^listusers', views.listusers, name='listusers'),
    re_path(r'^delusers', views.delusers, name='delusers'),
    re_path(r'^passwordcheck$', views.password_check),
    re_path(r'^deleteuser/(?P<username>[a-z]+)', views.del_user),
    re_path(r'^whoami$', views.whoami),
    re_path(r'^count$', views.count_user_view),
    re_path(r'^editprofile$', views.edit_profile),
]
