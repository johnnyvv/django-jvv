from django import forms
from django.forms import ModelForm
from myusers.models import CustomUser

class CreateUser(forms.Form):
    username = forms.CharField(help_text="",widget=forms.TextInput(attrs={'class':'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput()) 

class LoginUserForm(forms.Form):
    username = forms.CharField(help_text="",widget=forms.TextInput(attrs={'class':'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput()) 

class EditProfile(ModelForm):
    """
    #idea is to get the infof by username and them populate the form using html placeholder
    def __init__(self,username):
        self.username = username
        super().__init__()
                #doesnt belong here but under widgets 'last_name' :forms.TextInput(attrs={'placeholder': user.last_name}),
    """
    class Meta():
        model = CustomUser
       # user = CustomUser.objects.get(username__exact='johnnyz')
        fields  = ['first_name','last_name','email']
        widgets = {
                'password' : forms.PasswordInput(),
                }



"""
class AddUserForm(ModelForm):
    class Meta:
        model = CustomUser
        fields = ['username','password']
        help_texts = {
                'username': None,
                }
        widgets = {
                'username' : forms.TextInput(attrs={'class':'test class'}),
                'password' : forms.PasswordInput()
                }

class LoginUser(ModelForm):
    class Meta:
        model = CustomUser
        fields = ['username','password']
        help_texts = {
                'username': None,
                }
        widgets = {
                'username' : forms.TextInput(attrs={'class':'test class'}),
                'password' : forms.PasswordInput()
                }

"""
