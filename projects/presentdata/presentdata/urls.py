
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('myusers.urls')),
    path('devices/', include('devices.urls')),
    path('admin/', admin.site.urls),
    path('data/', include('datatest.urls')),
    ]
