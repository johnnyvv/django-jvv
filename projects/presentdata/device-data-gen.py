import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','presentdata.settings')
import django
django.setup()

##generate with faker
import random
from devices.models import Customer,NetworkDevice
from faker import Faker

fakegen = Faker()

def generate_customer(n=5):
    
    for run in range(n):
        company_name = fakegen.company()
        contact_person = fakegen.name()
        contact_email = fakegen.profile()['mail']
        Customer.objects.get_or_create(company=company_name,contact_person=contact_person,contact_email=contact_email)

def generate_device(n=10):
    companies = Customer.objects.values_list('company',flat=True)
    
    for run in range(n):
        company = random.choice(companies) #we need the PK here I think so to get that:
        #pk = 
        device_name = fakegen.hostname()
        ip_addr = fakegen.ipv4_private()
        kind = random.choice(['switch','router','adc'])
        vendor = random.choice(['cisco','f5','juniper','hp'])
        serial_number =  fakegen.isbn13(separator="-")
        rack =  random.choice(['c1','c2','c3','a1','a2','a3'])
        location =  fakegen.city()
        reg_date = fakegen.date(pattern="%Y-%m-%d")
        
        owner = Customer.objects.get(company__exact=company)
        #print(owner)
        NetworkDevice.objects.get_or_create(device_name=device_name,ip_addr=ip_addr,kind=kind,vendor=vendor,serial_number=serial_number,
        rack=rack,location=location, reg_date=reg_date,owner=owner)
        
def populate(n=5):
    for run in range(n):
        pass


if __name__ == '__main__':
    #gen customer
    #generate_customer(10)
    generate_device(20)