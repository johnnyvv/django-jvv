from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from django.views.generic import View,ListView,DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from .forms import CustomerForm, DeviceForm
from .models import Customer
from django.urls import reverse, reverse_lazy
import json 
# Create your views here.

def index(request):
    return HttpResponse("Welcome to the devices app")

def second(request):
    return HttpResponse("Welcome one step deeper.")

class SimplestView(View):
    def get(self, request, *args, **kwargs):
# Business logic goes here
        return HttpResponse('CBV')

class MyView(View):
    def get(self, request):
        return HttpResponse("Check")

class Daddy(View):
    text = "Just a bunch of text my friend"

    def get(self,request):
        return HttpResponse(self.text)

class Child(Daddy):
    pass # just make the class which inherits some stuff from Daddy

class CustomerAdd(View):
    form_class = CustomerForm
    title = "Customer"
    template_name = "devices/form_template.html"

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'title':'Customer', 'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            return HttpResponse("Success")
        return render(request, self.template_name, {'title':'Customer', 'form': form})

class DeviceAdd(View):
    form_class = DeviceForm
    title = "Network Device"
    template_name = "devices/form_template.html"
    url_message = None

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'title':self.title, 'form': form, 'url_message' : self.url_message})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            return HttpResponse("Success")
        return render(request, self.template_name, {'title':self.title, 'form': form})

class CustomerList(ListView):
    model = Customer
    context_object_name = "customer_list" #if you dont set this, you need to refer in your template to object object_list

class CustomerDetail():

    def get_context_data(self, **kwargs):
        #call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        print(context)
        context['testing'] = True
        return context

class CustomerDetailView(DetailView):
    template_name = "devices/customer_detail.html"
    queryset = Customer.objects.all() #can also limit this
    #queryset = Customer.objects.filter(id__exact=5) #so the only result you can get is where is = 5 since queryset is filled when server starts
    for x in queryset:
        print(x.id)
    #refer to object in your template as object or lowercase version of the model name
    #context_object_name = "testobject" #or explicit set it to a value of your own

def CustomerListDataTables(request):
    #dt_data = list(Customer.objects.values())
    dt_data = list(Customer.objects.values('company','contact_person','contact_email'))

    template = 'datatest/dt_model_json.html'
    if isinstance(dt_data[0], dict):
        dt_data_headers = list(dt_data[0].keys()) #so wont generate if dt_data is empty
    else:
        return HttpResponse("Failed to get table headers, since value is not a dictionary.")
    
    return HttpResponse("Here am I")
    #return render(request, template, {
    #   'datatables_array': json_dumps(dt_data),
    #    'datables_headers':dt_data_headers,
    #})

"""
    def get_object(self): #if you use this, then you dont need the queryset above
        id_ = self.kwargs.get("id")
        return get_object_or_404(Customer, id=id_)
"""

def CustomerListJson(request):
    qs = Customer.objects.values()
    ret = serializers.serialize('json', Customer.objects.all())
    return HttpResponse(ret, content_type='application/json')


class CustomerListTest(ListView):
    template_name = "devices/dt_table.html"
    model = Customer
    context_object_name = "customer_list" #if you dont set this, you need to refer in your template to object object_list
    
class UpdateViewCustomer(UpdateView):
    model = Customer
    template_name = "devices/update_customer.html"
    fields = ["company", "contact_person", "contact_email"]
    success_url =  reverse_lazy('customer_list')

class DeleteViewCustomer(DeleteView):
    model = Customer
    template_name = "devices/delete_customer.html"
    success_url =  reverse_lazy('customer_list')

class CreateViewCustomer(CreateView):
    model = Customer
    template_name = "devices/create_customer.html"
    fields = ["company", "contact_person", "contact_email"]
    success_url =  reverse_lazy('customer_list')