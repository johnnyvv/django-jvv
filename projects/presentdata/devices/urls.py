from django.urls import path, re_path
from django.views.generic import TemplateView #need to figure out how to send some context like title to the template
from . import views
from .views import SimplestView, MyView, Child, CustomerAdd, DeviceAdd, CustomerList, CustomerDetailView, CustomerListTest, UpdateViewCustomer, DeleteViewCustomer, CreateViewCustomer
#if using cbf you need to import the classs


urlpatterns = [
    path('', views.index),
    re_path(r'^second$', views.second),
    re_path(r'^x$', SimplestView.as_view()),
    re_path(r'^y$', MyView.as_view()),
    re_path(r'^about$', TemplateView.as_view(template_name="devices/about.html")),
    re_path(r'^child$', Child.as_view(text="Text from url conf yo")),
    re_path(r'^customer/add$', CustomerAdd.as_view()),
    re_path(r'^customer/list$', CustomerList.as_view()),
    re_path(r'^customer/dt/list$', CustomerListTest.as_view(), name="customer_list"),
    re_path(r'^customer/dt/json$', views.CustomerListJson),
    re_path(r'^customer/dt/update/(?P<pk>\d+)', UpdateViewCustomer.as_view(), name="customer_update"),
    re_path(r'^customer/dt/delete/(?P<pk>\d+)', DeleteViewCustomer.as_view(), name="customer_delete"),
    re_path(r'^customer/dt/create$', CreateViewCustomer.as_view(), name="customer_create"),
    path('customer/<int:pk>', CustomerDetailView.as_view(), name="customer_detail"),
    re_path(r'^device/add$', DeviceAdd.as_view(title="Network url")),
    #re_path(r'^devices', views.get_devices),
    #re_path(r'^customers',views.get_customers),
]