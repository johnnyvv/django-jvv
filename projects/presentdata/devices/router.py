class MyRouter():

    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'devices':
            #print(model._meta.app_label," for read") #careful this might end up in your variable!
            return 'network_db'
        return None
    
    def db_for_write(self, model, **hints):
        #print(f"trying to write model {model._meta.app_label}")
        if model._meta.app_label == 'devices':
            #print(model._meta.app_label," for write") #to test this import model and <Mdel>objects.create()
            return 'network_db'
        return None

    def allow_relation(self, obj1,obj2, **hints):
        if obj1._meta.app_label == 'devices' and obj2._meta.app_label == 'devices':
            return True
        return None
    
    def allow_migrate(self, db, app_label, model_name=None, **hints):
        #print(f"migrate: app is {app_label}")
        if app_label == 'devices': #and db  != 'default':
            #print(f"app is devices! model is {model_name} in database {db}")
            #print(model_name)
            #print(db)
            return db == 'network_db'
            #return True
        #elif db =='network db':
        #    print(f"db is <{db}>")
         #   return False
        return False #Try with None what is the difference?
        