from django.db import models

# Create your models here.
# we will create a model to put some generic info about lets say a router in the database

class Customer(models.Model):
    company = models.CharField(max_length=30) #not sure if I need to add primary key true here..
    contact_person = models.CharField(max_length=60)
    contact_email = models.EmailField()

    class Meta:
        ordering = ['company']
        #db_table = "jvv_customers"

    def __str__(self):
        return self.company

class NetworkDevice(models.Model):
    device_name = models.CharField(max_length=40,unique=True)
    ip_addr = models.GenericIPAddressField(unique=True, protocol='IPv4') 
    kind = models.CharField(max_length=20)
    vendor = models.CharField(max_length=30)
    serial_number =  models.CharField(max_length=40,unique=True)
    rack =  models.CharField(max_length=20,unique=False)
    location =  models.CharField(max_length=30,unique=False)
    reg_date = models.DateField()
    owner = models.ForeignKey(Customer, on_delete=models.CASCADE)
    #owner = models.CharField(max_length=30) #perhaps foreignkey of some model?
    
    class Meta:
        pass
        #app_label = 'devices'  #if you dont add an app to INSTALLED_APPS you need to define this here
        #db_table = 'jvv_network_devices'

    def __str__(self):
        return self.device_name

#x = Customer()
#print(x._meta.app_label)