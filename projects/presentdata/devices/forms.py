from django import forms
from django.forms import ModelForm, ChoiceField

from .models import Customer, NetworkDevice

#Create a customer form

class CustomerForm(ModelForm):
    class Meta:
        model = Customer
        fields = ['company','contact_person','contact_email']

class DeviceForm(ModelForm):
    device_choices = [('router','router'), ('adc','adc'),('switch','switch'), ('loadbalancer','loadbalancer')] #ofc we can do this in the database
    #vendor_choices = #plan is to auto fill dropdown based on choice e.g. if adc vendor gets only to be f5 or cisco for this we most likely need jquery or ajax
    kind = ChoiceField(choices=device_choices)  
    class Meta:
        model = NetworkDevice      
        exclude = ['id'] 
        # you need to either pass exclude or fields. Exclude will only show the fields that are not in the array
        #with fields = ['x'] #you can set which fields you want to show
