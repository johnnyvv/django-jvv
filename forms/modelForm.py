from django import forms
from django.forms import ModelForm
from myusers.models import CustomUser

class LoginUser(ModelForm):
    class Meta:
        model = CustomUser
        fields = ['username','password']
        help_texts = {
                'username': None,
                }
        widgets = {
                'username' : forms.TextInput(attrs={'class':'test class'}),
                'password' : forms.PasswordInput()
                }
