from django.http import HttpResponse
from django.shortcuts import render
from first_app.models import Webpage,Topic
from first_app.return_data import return_data_set
# Create your views here.

def index(request):
    return HttpResponse("Welcome to index of first App")

def spitvalues(request, data_mode):
    data_set = return_data_set(data_mode)
    dic = {'data_mode':data_mode,'data_set':data_set}
    print(dic)
    return render(request, 'first_app/spit.html', dic)