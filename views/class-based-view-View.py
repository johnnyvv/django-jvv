from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from .forms import DeviceForm

class DeviceAdd(View):
    form_class = DeviceForm
    title = "Network Device"
    template_name = "devices/form_template.html"

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'title':self.title, 'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            return HttpResponse("Success")
        return render(request, self.template_name, {'title':self.title, 'form': form})
        
        
        
#In your urls
"""
from .views import DeviceAdd

re_path(r'^device/add$', DeviceAdd.as_view()),

"""