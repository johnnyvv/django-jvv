from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View, ListView
from .models import Customer

class CustomerList(ListView):
    model = Customer
    context_object_name = "customer_list" #if you dont set this, you need to refer in your template to object object_list

#in views
# from .views import CustomerList
# re_path(r'^customer/list$', CustomerList.as_view()),
