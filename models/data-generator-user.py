import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','ex1.settings')
import django
django.setup()

##generate with faker
import random
from users.models import User
from faker import Faker

fakegen = Faker()

def populate(n=5):
    for run in range(n):

        first_name = fakegen.first_name()
        last_name = fakegen.last_name()
        username = fakegen.profile()['username']
        email = fakegen.email()
        reg_date = fakegen.date()

        create_user = User.objects.get_or_create(first_name=first_name,last_name=last_name,reg_date=reg_date,username=username,email=email)[0]

if __name__ == '__main__':
    print("Generating and populating...")
    populate(10)
    print("Done")