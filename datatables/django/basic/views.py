from django.shortcuts import render

from json import dumps as json_dumps
# Create your views here.


def datatables_dynamic_json(request):
    dt_data = [
        {'name':'johnny','age':28,'city':'Purmerend','eyes':'green',},
        {'name':'tina','age':35,'city':'Bandung','eyes':'brown',},
        {'name':'roos','age':58,'city':'Purmerend','eyes':'blueies',},
    ]    
    template = 'datatest/dt_dynamic_json.html'
    if isinstance(dt_data[0], dict):
        dt_data_headers = list(dt_data[0].keys()) #so wont generate if dt_data is empty
    else:
        return HttpResponse("First element is not a dictionary, hence there is no way to get the headers")

    print(dt_data_headers)
    return render(request, template, {
        'datatables_array': json_dumps(dt_data),
        'datables_headers':dt_data_headers,
    })
