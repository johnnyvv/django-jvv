##views.py
class UpdateViewCustomer(UpdateView):
    model = Customer
    template_name = "devices/update_customer.html"
    fields = ["company", "contact_person", "contact_email"]
    success_url =  reverse_lazy('customer_list')

#urls.py
re_path(r'^customer/dt/update/(?P<pk>\d+)', UpdateViewCustomer.as_view(), name="customer_update"),

#template
{% extends 'devices/base.html' %}
{% block title %}{{title}}{% endblock %}
{% block content %}
<h2>Udate customer <custoer name></h2>
    
<form method="post">{% csrf_token %}
    {{ form.as_p }}
    <input type="submit" value="Update">
</form>
{% endblock %}