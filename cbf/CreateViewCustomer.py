#view.py
class CreateViewCustomer(CreateView):
    model = Customer
    template_name = "devices/create_customer.html"
    fields = ["company", "contact_person", "contact_email"]
    success_url =  reverse_lazy('customer_list')

#urls.py
re_path(r'^customer/dt/create$', CreateViewCustomer.as_view(), name="customer_create"),

#template
{% extends 'devices/base.html' %}
{% block title %}{{title}}{% endblock %}
{% block content %}
<h2>Create customer</h2>
    
<form method="post">{% csrf_token %}
    {{form.as_p}}
    <input type="submit" value="Create">
</form>
{% endblock %}