#views.py
class CustomerDetailView(DetailView):
    template_name = "devices/customer_detail.html"
    queryset = Customer.objects.all() #can also limit this
    #queryset = Customer.objects.filter(id__exact=5) #so the only result you can get is where is = 5 since queryset is filled when server starts
    #for x in queryset:
    #    print(x.id)
    #refer to object in your template as object or lowercase version of the model name
    #context_object_name = "testobject" #or explicit set it to a value of your own
#urls.py
path('customer/<int:pk>', CustomerDetailView.as_view(), name="customer_detail"),

#template
{% extends 'devices/base.html' %}
{% block title %}{{title}}{% endblock %}
{% block content %}
<h2>Contact page for {{customer.company}}</h2>
<table> 
    <tr>
    <th>Company name</th>
    <th>Contact Person</th>
    <th>contact email</th>
    <th>contact id</th>
</tr>

    <tr>
        <td>{{customer.company}}</td>
        <td>{{customer.contact_person}}</td>
        <td>{{customer.contact_email}}</td>
        <td>{{customer.id}}</td>
    </tr>
    </table>


</table>
{% endblock %}