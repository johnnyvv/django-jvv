class Computer():
    def __init__(self, computer, ram, ssd):
        self.computer = computer
        self.ram = ram
        self.ssd = ssd
    def get_ram(self):
        return self.ram

    def summary(self):
        return "specs: Computer {} ram: {}GB, hdd size: {}".format(self.computer,self.ram,self.ssd)

class Laptop(Computer):
    def __init__(self, computer, ram, ssd, model):
        super().__init__(computer, ram, ssd)
        self.model = model
    

device = Laptop('Dell','8','512','lat')
print(device.computer)
print(device.ram)
print(device.ssd)
print(device.model)
print(device.get_ram())
print(device.summary())
