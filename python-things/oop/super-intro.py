#!/bin/env python3

#create the parent class
class Parent():
    def __init__(self):
        print("Parent")



class Child(Parent):
    def __init__(self):
        super().__init__()
        print("Child")

def main():
    x = Child()
    print(x)


if __name__ == '__main__':
    main()
