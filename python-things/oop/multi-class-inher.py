class A():
    def __init__(self):
        print('init class A')
    def sub_method(self,b):
        print('Print from class A:',b)

class B(A):
    def __init__(self):
        print('init class B')
        super().__init__()

    def sub_method(self,b):
        print('Print from class B:',b)
        super().sub_method(b+1)

class C(B):
    def __init__(self):
        print('init class C')
        super().__init__()
    
    def sub_method(self, b):
        print('Print from class C:',b)
        super().sub_method(b+1)

if __name__ == '__main__':
    c = C()
    c.sub_method(1)
