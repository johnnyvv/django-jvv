from django.contrib import admin
from django.urls import path,re_path,include
from first_app import urls
urlpatterns = [
    re_path(r'^',include('first_app.urls')),
    path('admin/', admin.site.urls),
]