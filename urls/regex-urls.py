from django.conf.urls import url
from first_app import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^twee/',views.print, name='printme'),
    url(r'^(?P<number>[0-9]{1,6})$',views.printnumber,name='printnumber'),
    url(r'^(?P<firstpart>[a-z]{1,10})/(?P<secondpart>[a-z]{1,3})$',views.doubleprint,name='doubleprint'),
    ]
##another example, dont just copy paste mkay
urlpatterns = [
    path('', views.index,name='index'),
    path('spit/<str:data_mode>', views.spitvalues),
    #re_path(r'^spit/(?P<model>^[A-Z][a-z])', views.spitvalues)
]